variable "name_prefix" {
  type = string
}

variable "datastore_id" {
  type = string
}

variable "folder" {
  type = string
}

variable "pool_id" {
  type = string
}

variable "ip_list" {
  type = list(string)
}

variable "ipv4_netmask" {
  type = number
}

variable "network_id" {
  type = string
}

variable "ipv4_gateway" {
  type = string
}

variable "dns_server_list" {
  type = list(string)
}

variable "domain" {
  type = string
}

variable "num_cpu" {
  type = number
}

variable "memory" {
  type = number
}

variable "disk_size" {
  type = number
}

variable "guest_id" {
  type = string
}

variable "template_uuid" {
  type = string
}
