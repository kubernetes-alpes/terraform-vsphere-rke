resource "vsphere_virtual_machine" "vm" {
  for_each         = {
    for index, ip in var.ip_list: index => ip
  }
  name             = "${var.name_prefix}-${format("%03d", each.key + 1)}"
  resource_pool_id = var.pool_id
  datastore_id     = var.datastore_id
  folder           = var.folder
  num_cpus         = var.num_cpu
  memory           = var.memory
  guest_id         = var.guest_id

  network_interface {
    network_id = var.network_id
  }

  disk {
    label = "disk0"
    size  = var.disk_size
  }

  clone {
    template_uuid = var.template_uuid

    customize {
      network_interface {
        ipv4_address = each.value
        ipv4_netmask = var.ipv4_netmask
      }
      ipv4_gateway = var.ipv4_gateway
      dns_server_list = var.dns_server_list
      linux_options {
        host_name = "${var.name_prefix}-${format("%03d", each.key + 1)}"
        domain = var.domain
      }
    }

  }
}
