variable "master_depends_on" {
  type = list
}

variable "worker_depends_on" {
  type = list
}

variable "cluster_name" {
  type = string
}

variable "master_ip_list" {
  type = list(string)
}

variable "worker_ip_list" {
  type = list(string)
}

variable "system_user" {
  type = string
}

variable "ssh_key_file" {
  type = string
}

variable "use_ssh_agent" {
  type    = bool
  default = "true"
}

variable "wait_for_commands" {
  type = list(string)
}

variable "master_labels" {
  type = map(string)
}

variable "worker_labels" {
  type = map(string)
}

variable "master_taints" {
  type = list(map(string))
}

variable "worker_taints" {
  type = list(map(string))
}

variable "k8s_version" {
  type = string
}

variable "mtu" {
  type    = number
  default = 0
}

variable "deploy_nginx" {
  type = bool
}

variable "cloud_provider" {
  type = bool
}

variable "write_kubeconfig" {
  type = bool
}
