resource "null_resource" "wait_for_master_ssh" {
  depends_on   = [var.master_depends_on]
  for_each         = {
    for index, ip in var.master_ip_list: index => ip
  }
  connection {
    host        = each.value
    user        = var.system_user
    private_key = var.use_ssh_agent ? null : file(var.ssh_key_file)
    agent       = var.use_ssh_agent
  }
  provisioner "remote-exec" {
    inline = var.wait_for_commands
  }
}

resource "null_resource" "wait_for_worker_ssh" {
  depends_on   = [var.worker_depends_on]
  for_each         = {
    for index, ip in var.worker_ip_list: index => ip
}
  connection {
    host         = each.value
    user         = var.system_user
    private_key  = var.use_ssh_agent ? null : file(var.ssh_key_file)
    agent        = var.use_ssh_agent
  }
  provisioner "remote-exec" {
    inline = var.wait_for_commands
  }
}

resource "rke_cluster" "cluster" {

  depends_on   = [null_resource.wait_for_master_ssh,
  null_resource.wait_for_worker_ssh]
  cluster_name = var.cluster_name

  dynamic nodes {
    for_each         = {
      for index, ip in var.master_ip_list: index => ip
    }
    content {
      address           = nodes.value
      internal_address  = nodes.value
#      hostname_override = nodes.key
      user              = var.system_user
      role              = ["controlplane", "etcd"]
      labels            = var.master_labels
      dynamic taints {
        for_each = var.master_taints
        content {
          key    = lookup(taints.value, "key")
          value  = lookup(taints.value, "value")
          effect = lookup(taints.value, "effect", "NoSchedule")
        }
      }
    }
  }

  dynamic nodes {
    for_each         = {
      for index, ip in var.worker_ip_list: index => ip
    }
    content {
      address           = nodes.value
      internal_address  = nodes.value
#      hostname_override = "${var.name_prefix}-${format("%03d", each.key + 1)}"
      user              = var.system_user
      role              = ["worker"]
      labels            = var.worker_labels
      dynamic taints {
        for_each = var.worker_taints
        content {
          key    = lookup(taints.value, "key")
          value  = lookup(taints.value, "value")
          effect = lookup(taints.value, "effect", "NoSchedule")
        }
      }
    }
  }

  ingress {
    provider      = var.deploy_nginx ? "nginx" : "none"
  }

  ssh_agent_auth = var.use_ssh_agent
  ssh_key_path   = var.ssh_key_file

  kubernetes_version = var.k8s_version

  network {
    mtu = var.mtu
  }

}

resource "local_file" "kube_cluster_yaml" {
  count             = var.write_kubeconfig ? 1 : 0
  filename          = "${path.root}/kube_config_cluster.yml"
  sensitive_content = rke_cluster.cluster.kube_config_yaml
}
