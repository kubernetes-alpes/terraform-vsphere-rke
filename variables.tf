variable "cluster_name" {
  type        = string
  default     = "osug-rke"
  description = "Cluster name"
}

variable "master_ip_list" {
  type        = list(string)
  description = "A list of master/etcd nodes IP"
}

variable "worker_ip_list" {
  type        = list(string)
  description = "A list of worker nodes IP"
}

variable "ipv4_netmask" {
  type        = number
  description = "The IPV4 subnet mask in bits (e.g. 24 for 255.255.255.0)"
}

variable "ipv4_gateway" {
  type        = string
  description = "The IPv4 default gateway"
}

variable "dns_server_list" {
  type        = list(string)
  description = " The list of DNS servers to configure on a virtual machine. "
}

variable "domain" {
  type        = string
  description = "A list of DNS search domains to add to the DNS configuration on the virtual machine"
}

variable "datacenter" {
  type        = string
  description = "The vSphere datacenter name"
}

variable "datastore" {
  type        = string
  description = "The vSphere datastore name"
}

variable "pool" {
  type        = string
  description = "The vSphere resource pool name"
}

variable "folder" {
  type        = string
  description = "The path to the folder to put this virtual machine in"
  default     = "." 
}

variable "network" {
  type        = string
  description = "The vSphere network name"
}

variable "template_uuid" {
  type        = string
  description = "The VM template UUID"
}

variable "guest_id" {
  type        = string
  description = "The VM guest ID"
}

variable "master_num_cpu" {
  type        = number
  description = "CPU count for master nodes"
}

variable "master_memory" {
  type        = number
  description = "Memory count for master nodes"
}

variable "master_disk_size" {
  type        = number
  description = "Master nodes disk size"
}

variable "worker_num_cpu" {
  type        = number
  description = "CPU count for worker nodes"
}

variable "worker_memory" {
  type        = number
  description = "Memory count for worker nodes"
}

variable "worker_disk_size" {
  type        = number
  description = "Worker nodes disk size"
}

#################
# RKE variables #
#################

variable "system_user" {
  type        = string
  default     = "ubuntu"
  description = "Default OS image user"
}

variable "use_ssh_agent" {
  type        = bool
  default     = "true"
  description = "Whether to use ssh agent"
}

variable "ssh_key_file" {
  type        = string
  default     = "~/.ssh/id_rsa"
  description = "Local path to SSH key"
}

variable "wait_for_commands" {
  type        = list(string)
  default     = ["# Connected !"]
  description = "Commands to run on nodes before running RKE"
}

variable "master_labels" {
  type        = map(string)
  default     = { "node-role.kubernetes.io/master" = "true" }
  description = "Master labels"
}

variable "worker_labels" {
  type        = map(string)
  default     = { "node-role.kubernetes.io/worker" = "true" }
  description = "Worker labels"
}

variable "master_taints" {
  type        = list(map(string))
  default     = []
  description = "Master taints"
}

variable "worker_taints" {
  type        = list(map(string))
  default     = []
  description = "Worker taints"
}

variable "kubernetes_version" {
  type        = string
  default     = null
  description = "Kubernetes version (RKE)"
}

variable "cni_mtu" {
  type        = number
  default     = 0
  description = "CNI MTU"
}

variable "deploy_nginx" {
  type        = bool
  default     = "false"
  description = "Whether to deploy nginx RKE addon"
}

variable "cloud_provider" {
  type        = bool
  default     = "true"
  description = "Deploy cloud provider"
}

variable "default_storage" {
  type        = string
  default     = null
  description = "Default storage class"
}

variable "addons_include" {
  type        = list(string)
  default     = null
  description = "RKE YAML files for add-ons"
}

variable "write_kubeconfig" {
  type        = bool
  default     = "true"
  description = "Write kubeconfig file to disk"
}

