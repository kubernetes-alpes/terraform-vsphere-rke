output "cluster_name" {
  value       = var.cluster_name
  description = "Cluster name"
}


output "worker_ip_list" {
  value       = var.worker_ip_list
  description = "Workers ip list"
}

output "rke_cluster" {
  value       = module.rke.rke_cluster
  description = "RKE cluster spec"
  sensitive   = "true"
}
