## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13 |
| local | >=1.4.0 |
| null | >=2.1.2 |
| rke | >=1.1.0 |
| vsphere | ~> 1.24.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| cluster\_name | Cluster name | `string` | `"osug-rke"` | no |
| master\_ip\_list | A list of master/etcd nodes IP | `list(string)` | n/a | yes |
| worker\_ip\_list | A list of worker nodes IP | `list(string)` | n/a | yes |
| ipv4\_netmask | The IPV4 subnet mask in bits (e.g. 24 for 255.255.255.0) | `number` | n/a | yes |
| ipv4\_gateway | The IPv4 default gateway | `string` | n/a | yes |
| dns\_server\_list | The list of DNS servers to configure on a virtual machine. | `list(string)` | n/a | yes |
| domain | A list of DNS search domains to add to the DNS configuration on the virtual machine | `string` | n/a | yes |
| datacenter | The vSphere datacenter name | `string` | n/a | yes |
| datastore | The vSphere datastore name | `string` | n/a | yes |
| pool | The vSphere resource pool name | `string` | n/a | yes |
| folder | The path to the folder to put this virtual machine in | `string` | `"."` | no |
| network | The vSphere network name | `string` | n/a | yes |
| template\_uuid | The VM template UUID | `string` | n/a | yes |
| guest\_id | The VM guest ID | `string` | n/a | yes |
| master\_num\_cpu | CPU count for master nodes | `number` | n/a | yes |
| master\_memory | Memory count for master nodes | `number` | n/a | yes |
| master\_disk\_size | Master nodes disk size | `number` | n/a | yes |
| worker\_num\_cpu | CPU count for worker nodes | `number` | n/a | yes |
| worker\_memory | Memory count for worker nodes | `number` | n/a | yes |
| worker\_disk\_size | Worker nodes disk size | `number` | n/a | yes |
| system\_user | Default OS image user | `string` | `"ubuntu"` | no |
| use\_ssh\_agent | Whether to use ssh agent | `bool` | `"true"` | no |
| ssh\_key\_file | Local path to SSH key | `string` | `"~/.ssh/id_rsa"` | no |
| wait\_for\_commands | Commands to run on nodes before running RKE | `list(string)` | <pre>[<br>  "# Connected !"<br>]</pre> | no |
| master\_labels | Master labels | `map(string)` | <pre>{<br>  "node-role.kubernetes.io/master": "true"<br>}</pre> | no |
| worker\_labels | Worker labels | `map(string)` | <pre>{<br>  "node-role.kubernetes.io/worker": "true"<br>}</pre> | no |
| master\_taints | Master taints | `list(map(string))` | `[]` | no |
| worker\_taints | Worker taints | `list(map(string))` | `[]` | no |
| kubernetes\_version | Kubernetes version (RKE) | `string` | `null` | no |
| cni\_mtu | CNI MTU | `number` | `0` | no |
| deploy\_nginx | Whether to deploy nginx RKE addon | `bool` | `"false"` | no |
| cloud\_provider | Deploy cloud provider | `bool` | `"true"` | no |
| default\_storage | Default storage class | `string` | `null` | no |
| addons\_include | RKE YAML files for add-ons | `list(string)` | `null` | no |
| write\_kubeconfig | Write kubeconfig file to disk | `bool` | `"true"` | no |

## Outputs

| Name | Description |
|------|-------------|
| cluster\_name | Cluster name |
| worker\_ip\_list | Workers ip list |
| rke\_cluster | RKE cluster spec |

