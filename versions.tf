terraform {
  required_providers {
    local = {
      source  = "hashicorp/local"
      version = ">=1.4.0"

    }
    null = {
      source  = "hashicorp/null"
      version = ">=2.1.2"
    }
    rke = {
      source  = "rancher/rke"
      version = ">=1.1.0"
    }
    vsphere = {
      source  = "hashicorp/vsphere"
      version = "~> 1.24.0"
    }
  }
  required_version = ">= 0.13"
}
