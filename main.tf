data "vsphere_datacenter" "dc" {
  name = var.datacenter
}

data "vsphere_datastore" "datastore" {
  name          = var.datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name          = var.pool
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "network" {
  name          = var.network
  datacenter_id = data.vsphere_datacenter.dc.id
}

module "master" {
  source          = "./modules/node"
  name_prefix     = "${var.cluster_name}-master"
  ip_list         = var.master_ip_list
  ipv4_netmask    = var.ipv4_netmask
  ipv4_gateway    = var.ipv4_gateway
  dns_server_list = var.dns_server_list
  domain          = var.domain
  template_uuid   = var.template_uuid
  num_cpu         = var.master_num_cpu
  disk_size       = var.master_disk_size
  memory          = var.master_memory
  guest_id        = var.guest_id
  folder          = var.folder
  pool_id         = data.vsphere_resource_pool.pool.id
  datastore_id    = data.vsphere_datastore.datastore.id
  network_id      = data.vsphere_network.network.id
}

module "worker" {
  source          = "./modules/node"
  name_prefix     = "${var.cluster_name}-worker"
  ip_list         = var.worker_ip_list
  ipv4_netmask    = var.ipv4_netmask
  ipv4_gateway    = var.ipv4_gateway
  dns_server_list = var.dns_server_list
  domain          = var.domain
  template_uuid   = var.template_uuid
  num_cpu         = var.worker_num_cpu
  disk_size       = var.worker_disk_size
  memory          = var.worker_memory
  guest_id        = var.guest_id
  folder          = var.folder
  pool_id         = data.vsphere_resource_pool.pool.id
  datastore_id    = data.vsphere_datastore.datastore.id
  network_id      = data.vsphere_network.network.id
}

module "rke" {
  source            = "./modules/rke"
  master_depends_on = [module.master]
  worker_depends_on = [module.worker]
  cluster_name      = var.cluster_name
  master_ip_list    = var.master_ip_list
  worker_ip_list    = var.worker_ip_list
  system_user       = var.system_user
  ssh_key_file      = var.ssh_key_file
  use_ssh_agent     = var.use_ssh_agent
  wait_for_commands = var.wait_for_commands
  master_labels     = var.master_labels
  worker_labels     = var.worker_labels
  master_taints     = var.master_taints
  worker_taints     = var.worker_taints
  k8s_version       = var.kubernetes_version
  mtu               = var.cni_mtu
  deploy_nginx      = var.deploy_nginx
  cloud_provider    = var.cloud_provider
  write_kubeconfig  = var.write_kubeconfig
}

